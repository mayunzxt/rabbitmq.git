/*
Navicat MySQL Data Transfer

Source Server         : localhost_mysql
Source Server Version : 50727
Source Host           : localhost:3306
Source Database       : zmq

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2022-07-25 10:59:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for message_receive_log
-- ----------------------------
DROP TABLE IF EXISTS `message_receive_log`;
CREATE TABLE `message_receive_log` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `uri` varchar(32) DEFAULT NULL COMMENT '消息报文唯一标识',
  `receive_msg` text COMMENT '接收数据',
  `receive_time` datetime DEFAULT NULL COMMENT '接收时间',
  `status` char(1) DEFAULT NULL COMMENT '状态(0未确认1 已确认)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接收消息记录';

-- ----------------------------
-- Records of message_receive_log
-- ----------------------------
INSERT INTO `message_receive_log` VALUES ('296df0593347fbf08f2add0ee9629da9', 'fb6ce09044dd4423a15bc3d523377d81', 'Hello MQ!', '2022-07-25 10:54:47', '1', '2022-07-25 10:54:47', '2022-07-25 10:54:47');
INSERT INTO `message_receive_log` VALUES ('6283eb46ccb74574b7b0021c921478ab', '622a352dc5924be29db3928dfa111552', 'Hello MQ!', '2022-07-25 10:50:12', '1', '2022-07-25 10:50:12', '2022-07-25 10:50:12');

-- ----------------------------
-- Table structure for message_send_log
-- ----------------------------
DROP TABLE IF EXISTS `message_send_log`;
CREATE TABLE `message_send_log` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `uri` varchar(32) DEFAULT NULL COMMENT '消息报文唯一标识',
  `send_msg` text COMMENT '发送数据',
  `exchange_name` varchar(60) DEFAULT NULL COMMENT '交换机名称',
  `router_key` varchar(30) DEFAULT NULL COMMENT '路由',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `status` char(1) DEFAULT NULL COMMENT '状态(0发送失败 1 已发送 2发送成功)',
  `fail_reason` text COMMENT '失败原因',
  `req_uri` varchar(32) DEFAULT NULL COMMENT '回执的请求标识',
  `send_times` int(11) DEFAULT NULL COMMENT '已重发次数',
  `max_send` int(11) DEFAULT NULL COMMENT '重发次数',
  `interval_time` int(11) DEFAULT NULL COMMENT '重发时间间隔(秒)',
  `already_dead` char(1) DEFAULT NULL COMMENT '消息是否已死亡',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息记录__MessageSendLog';

-- ----------------------------
-- Records of message_send_log
-- ----------------------------
INSERT INTO `message_send_log` VALUES ('2874248cb19f63044dc2d0dffc825d04', '622a352dc5924be29db3928dfa111552', 'Hello MQ!', 'test001-1', 'routingKey001', '2022-07-25 10:50:12', '1', null, null, '1', null, null, '0', '2022-07-25 10:50:12', '2022-07-25 10:50:12');
INSERT INTO `message_send_log` VALUES ('aad475527443944b544ebe1e759ce6bb', 'fb6ce09044dd4423a15bc3d523377d81', 'Hello MQ!', 'test001-1', 'routingKey001', '2022-07-25 10:54:47', '1', null, null, '1', null, null, '0', '2022-07-25 10:54:47', '2022-07-25 10:54:47');
SET FOREIGN_KEY_CHECKS=1;
