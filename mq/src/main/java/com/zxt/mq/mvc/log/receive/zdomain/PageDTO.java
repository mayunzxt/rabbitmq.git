package com.zxt.mq.mvc.log.receive.zdomain;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分页器参数接收类
 * </p>
 *
 * @author zxt
 * @since 2022-07-25
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "PageDTO", description = "分页器参数接收类")
public class PageDTO {
    private int pageNum;
    private int pageSize;
}
