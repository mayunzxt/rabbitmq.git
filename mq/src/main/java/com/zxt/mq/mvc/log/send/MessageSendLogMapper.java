package com.zxt.mq.mvc.log.send;

import com.zxt.mq.mvc.log.send.zdomain.MessageSendLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 消息记录 Mapper 接口
 * </p>
 *
 * @author zxt
 * @since 2022-07-25
 */
@Mapper
public interface MessageSendLogMapper extends BaseMapper<MessageSendLog> {

}
