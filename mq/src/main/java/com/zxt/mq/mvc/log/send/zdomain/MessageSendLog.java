package com.zxt.mq.mvc.log.send.zdomain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 消息记录
 * </p>
 *
 * @author zxt
 * @since 2022-07-25
 */
@Data
@Accessors(chain = true)
@TableName("message_send_log")
@ApiModel(value = "MessageSendLog对象", description = "消息记录")
public class MessageSendLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 数据库常量字段 */
    @SuppressWarnings("all")
    public interface DbKey {
        /** 主键 */
        String id           = "id";
        /** 消息报文唯一标识 */
        String uri          = "uri";
        /** 发送数据 */
        String sendMsg      = "send_msg";
        /** 交换机名称 */
        String exchangeName = "exchange_name";
        /** 路由 */
        String routerKey    = "router_key";
        /** 发送时间 */
        String sendTime     = "send_time";
        /** 状态(0发送失败 1 已发送 2发送成功) */
        String status       = "status";
        /** 失败原因 */
        String failReason   = "fail_reason";
        /** 回执的请求标识 */
        String reqUri       = "req_uri";
        /** 最大重发次数 */
        String maxSend      = "max_send";
        /** 重发时间间隔(秒) */
        String intervalTime = "interval_time";
        /** 已重发次数 */
        String sendTimes    = "send_times";
        /** 消息是否已死亡(0否 1 是) */
        String alreadyDead  = "already_dead";
        /** 创建时间 */
        String createTime   = "create_time";
        /** 更新时间 */
        String updateTime   = "update_time";
    }

    /** 主键 */
    @ApiModelProperty(value = "主键")
    private String        id;
    /** 消息报文唯一标识 */
    @ApiModelProperty(value = "消息报文唯一标识")
    private String        uri;
    /** 发送数据 */
    @ApiModelProperty(value = "发送数据")
    private String        sendMsg;
    /** 交换机名称 */
    @ApiModelProperty(value = "交换机名称")
    private String        exchangeName;
    /** 路由 */
    @ApiModelProperty(value = "路由")
    private String        routerKey;
    /** 发送时间 */
    @ApiModelProperty(value = "发送时间")
    private LocalDateTime sendTime;
    /** 状态(0发送失败 1 已发送 2发送成功) */
    @ApiModelProperty(value = "状态(0发送失败 1 已发送 2发送成功)")
    private String        status;
    /** 失败原因 */
    @ApiModelProperty(value = "失败原因")
    private String        failReason;
    /** 回执的请求标识 */
    @ApiModelProperty(value = "回执的请求标识")
    private String        reqUri;
    /** 已重发次数 */
    @ApiModelProperty(value = "已重发次数")
    private Integer       sendTimes;
    /** 最大重发次数 */
    @ApiModelProperty(value = "最大重发次数")
    private Integer       maxSend;
    /** 重发时间间隔(秒) */
    @ApiModelProperty(value = "重发时间间隔(秒)")
    private Integer       intervalTime;
    /** 消息是否已死亡(0否 1 是) */
    @ApiModelProperty(value = "消息是否已死亡(0否 1 是)")
    private String        alreadyDead;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
