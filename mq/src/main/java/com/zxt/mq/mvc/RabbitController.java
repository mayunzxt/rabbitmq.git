package com.zxt.mq.mvc;

import com.zstk.frame.MvcResult;
import com.zxt.mq.config.rabbitmq.RabbitUtil;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * rabbitmq控制器
 * </p>
 *
 * @author zxt
 * @since 2022/6/22 17:55
 */
@RestController
@RequestMapping("/rabbit")
@Api(tags = "rabbitmq接口")
public class RabbitController {
    @Resource
    RabbitUtil rabbitUtil;


    @ApiOperation(value = "获取连接参数")
    @PostMapping("/connect")
    public MvcResult getConnect() {
        return MvcResult.success().data(rabbitUtil.getFrontConnect());
    }


}
