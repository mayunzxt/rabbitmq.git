package com.zxt.mq.mvc.log.send;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zxt.mq.config.rabbitmq.RabbitData;
import com.zxt.mq.mvc.log.send.zdomain.MessageSendLog;

import java.util.List;

/**
 * <p>
 * 消息记录 服务类
 * </p>
 *
 * @author zxt
 * @since 2022-07-25
 */
@SuppressWarnings("MybatisMapperMethodInspection")
public interface MessageSendLogService extends IService<MessageSendLog> {

    /**
     * 保存发送记录
     *
     * @param rabbitData   发送数据
     * @param exchangeName 交换机名称
     * @param routerKey    路由
     */
    void saveLog(RabbitData rabbitData, String exchangeName, String routerKey);

    /**
     * 更新消息记录
     *
     * @param uri        业务消息唯一标识
     * @param status     状态
     * @param failReason 发送失败原因
     */

    void updateLog(String uri, String status, String failReason);

    /**
     * 查询消息记录
     *
     * @param uri 业务消息唯一标识
     *
     * @return 消息记录
     */
    MessageSendLog getByUri(String uri);

    /**
     * 设置消息为已死亡
     *
     * @param messageSendLog 消息对象
     */
    void setMessageToAlreadyDead(MessageSendLog messageSendLog);

    /**
     * 消息失败记录或未消费记录
     *
     * @return 消息失败记录或未消费记录
     */
    List<MessageSendLog> listFail();

}
