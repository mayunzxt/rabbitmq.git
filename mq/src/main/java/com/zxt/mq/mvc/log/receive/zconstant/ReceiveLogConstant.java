package com.zxt.mq.mvc.log.receive.zconstant;

/**
 * <p>
 * 消息接收状态常量
 * </p>
 *
 * @author ZColin
 * @since 2022/7/20 9:17
 */
public final class ReceiveLogConstant {
    private ReceiveLogConstant() {
    }

    /** 未确认 */
    public static final String UN_CONFIRM = "0";
    /** 已确认 */
    public static final String CONFIRM    = "1";
}