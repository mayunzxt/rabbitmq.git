package com.zxt.mq.mvc.log.receive;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zxt.mq.config.rabbitmq.RabbitData;
import com.zxt.mq.mvc.log.receive.zdomain.MessageReceiveLog;

import java.util.List;

/**
 * <p>
 * 接收消息记录 服务类
 * </p>
 *
 * @author zxt
 * @since 2022-07-25
 */
@SuppressWarnings("MybatisMapperMethodInspection")
public interface MessageReceiveLogService extends IService<MessageReceiveLog> {
    /**
     * 保存消息记录
     *
     * @param status     0 未确认 1 已确认
     * @param rabbitData 消息记录
     */
    void saveLog(RabbitData rabbitData, String status);

    /**
     * 查询消息记录
     *
     * @param uri 业务消息唯一标识
     *
     * @return 消息记录
     */
    MessageReceiveLog getByUri(String uri);

    /**
     * 未消息记录uri列表
     *
     * @return 未消息记录uri列表
     */
    List<String> listFailUri();
}
