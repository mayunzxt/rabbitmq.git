package com.zxt.mq.mvc.log.receive.zdomain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 接收消息记录
 * </p>
 *
 * @author zxt
 * @since 2022-07-25
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "MessageReceiveLog对象", description = "接收消息记录")
public class MessageReceiveLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 数据库常量字段 */
    @SuppressWarnings("all")
    public interface DbKey {
        /** 主键 */
        String id          = "id";
        /** 消息报文唯一标识 */
        String uri         = "uri";
        /** 接收数据 */
        String receiveMsg  = "receive_msg";
        /** 接收时间 */
        String receiveTime = "receive_time";
        /** 状态(0未确认1 已确认) */
        String status      = "status";
        /** 创建时间 */
        String createTime  = "create_time";
        /** 更新时间 */
        String updateTime  = "update_time";
    }

    /** 主键 */
    @ApiModelProperty(value = "主键")
    private String        id;
    /** 消息报文唯一标识 */
    @ApiModelProperty(value = "消息报文唯一标识")
    private String        uri;
    /** 接收数据 */
    @ApiModelProperty(value = "接收数据")
    private String        receiveMsg;
    /** 接收时间 */
    @ApiModelProperty(value = "接收时间")
    private LocalDateTime receiveTime;
    /** 状态(0未确认1 已确认) */
    @ApiModelProperty(value = "状态(0未确认1 已确认)")
    private String        status;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
