package com.zxt.mq.mvc.log.receive;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxt.mq.mvc.log.receive.zdomain.IdDTO;
import com.zxt.mq.mvc.log.receive.zdomain.MessageReceiveLog;
import com.zxt.mq.mvc.log.receive.zdomain.PageDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 接收消息记录 前端控制器
 * </p>
 *
 * @author zxt
 * @since 2022-07-25
 */
@Api(tags = "接收消息记录接口")
@RestController
@RequestMapping("/log/receive")
@Slf4j
public class MessageReceiveLogController {
    @Resource
    MessageReceiveLogService messageReceiveLogService;


    @ApiOperation(value = "保存接收记录")
    @PostMapping("/save")
    public Object save(@RequestBody MessageReceiveLog messageReceiveLog) {
        return messageReceiveLogService.saveOrUpdate(messageReceiveLog);
    }

    @ApiOperation(value = "删除接收消息记录")
    @PostMapping("/delete")
    public Object delete(@Valid @RequestBody IdDTO idDTO) {
        return messageReceiveLogService.removeById(idDTO.getId());
    }

    @ApiOperation(value = "获得接收消息记录详情")
    @PostMapping("/get")
    public Object getById(@Valid @RequestBody IdDTO idDTO) {
        return messageReceiveLogService.getById(idDTO.getId());
    }

    @ApiOperation(value = "接收记录分页")
    @PostMapping("/listbypage")
    public Object listByPage(@Valid @RequestBody PageDTO pageDTO) {
        return messageReceiveLogService.page(new Page<>(pageDTO.getPageNum(), pageDTO.getPageSize()));
    }
}
