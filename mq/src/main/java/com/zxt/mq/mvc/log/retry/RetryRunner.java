package com.zxt.mq.mvc.log.retry;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

/**
 * <p>
 * 服务启动后-消息重发任务
 * </p>
 *
 * @author zxt
 * @since 2022/6/6 13:29
 */
@Component
public class RetryRunner implements ApplicationRunner {
    @Value("${spring.rabbitmq.message-log.log}")
    private boolean log;
    @Value("${spring.rabbitmq.message-log.retry-send}")
    private boolean retry;
    @Value("${spring.rabbitmq.message-log.delay-time}")
    private int     delay;
    @Resource
    RetryService retryService;

    @Override
    public void run(ApplicationArguments args) {
        if (log && retry) {
            retryService.startResend(delay, TimeUnit.SECONDS);
        }

    }
}
