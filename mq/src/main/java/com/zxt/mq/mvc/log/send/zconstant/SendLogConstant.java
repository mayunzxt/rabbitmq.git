package com.zxt.mq.mvc.log.send.zconstant;

/**
 * <p>
 * 消息发送状态常量
 * </p>
 *
 * @author ZColin
 * @since 2022/7/20 9:17
 */
public final class SendLogConstant {
    private SendLogConstant() {
    }

    /** 发送失败 */
    public static final String SEND_FAIL    = "0";
    /** 发送成功 */
    public static final String SEND_SUCCESS = "1";
}
