package com.zxt.mq.mvc.log.receive;

import com.zxt.mq.mvc.log.receive.zdomain.MessageReceiveLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 接收消息记录 Mapper 接口
 * </p>
 *
 * @author zxt
 * @since 2022-07-25
 */
@Mapper
public interface MessageReceiveLogMapper extends BaseMapper<MessageReceiveLog> {

}
