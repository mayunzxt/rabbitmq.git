package com.zxt.mq.mvc.log.send;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxt.mq.mvc.log.receive.zdomain.IdDTO;
import com.zxt.mq.mvc.log.receive.zdomain.PageDTO;
import com.zxt.mq.mvc.log.send.zdomain.MessageSendLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 消息发送记录 前端控制器
 * </p>
 *
 * @author zxt
 * @since 2022-07-25
 */
@Api(tags = "消息发送记录接口")
@RestController
@RequestMapping("/log/send")
@Slf4j
public class MessageSendLogController {
    @Resource
    MessageSendLogService messageSendLogService;


    @ApiOperation(value = "保存发送记录")
    @PostMapping("/save")
    public Object save(@RequestBody MessageSendLog messageSendLog) {
        return messageSendLogService.saveOrUpdate(messageSendLog);
    }

    @ApiOperation(value = "删除消息记录")
    @PostMapping("/delete")
    public Object delete(@Valid @RequestBody IdDTO idDTO) {
        return messageSendLogService.removeById(idDTO.getId());
    }

    @ApiOperation(value = "获得消息记录详情")
    @PostMapping("/get")
    public Object getById(@Valid @RequestBody IdDTO idDTO) {
        return messageSendLogService.getById(idDTO.getId());
    }

    @ApiOperation(value = "发送记录分页")
    @PostMapping("/listbypage")
    public Object listByPage(@Valid @RequestBody PageDTO pageDTO) {
        return messageSendLogService.page(new Page<>(pageDTO.getPageNum(), pageDTO.getPageSize()));
    }
}
