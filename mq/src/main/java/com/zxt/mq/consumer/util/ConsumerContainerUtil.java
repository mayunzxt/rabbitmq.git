package com.zxt.mq.consumer.util;

import com.zxt.mq.consumer.DynamicConsumerContainerFactory;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;

/**
 * <p>
 * 消费者监听管理器生成工具
 * </p>
 *
 * @author zxt
 * @since 2022/7/22 14:01
 */
public class ConsumerContainerUtil {
    /**
     * 动态生成消费者
     *
     * @param connectionFactory mq连接工程
     * @param rabbitAdmin       mq管理操作类
     * @param exchangeName      交换机名称
     * @param queueName         队列名称
     * @param routingKey        路由
     * @param autoDelete        是否自动删除
     * @param durable           是否持久化
     * @param autoAck           是否自动确认
     *
     * @return 动态消费者
     *
     * @throws Exception 异常
     */
    public static DynamicConsumerContainerFactory genConsumer(ConnectionFactory connectionFactory,
                                                              RabbitAdmin rabbitAdmin, String exchangeName, String queueName, String routingKey, boolean autoDelete,
                                                              boolean durable, boolean autoAck) throws Exception {
        return DynamicConsumerContainerFactory.builder()
                                              .directExchange(exchangeName)
                                              .queue(queueName)
                                              .autoDeleted(autoDelete)
                                              .autoAck(autoAck)
                                              .durable(durable)
                                              .routingKey(routingKey)
                                              .rabbitAdmin(rabbitAdmin)
                                              .connectionFactory(connectionFactory)
                                              .build();
    }
}
