package com.zxt.mq.consumer;

import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;

/**
 * <p>
 * 动态消费者监听接口
 * </p>
 *
 * @author zxt
 * @since 2022/7/22 11:37
 */
public interface IDynamicConsumer extends ChannelAwareMessageListener {

    /**
     * 设置消息监听管理
     *
     * @param container 管理器
     */
    void setContainer(SimpleMessageListenerContainer container);

    /**
     * 消费监听停止
     */
    default void shutdown() {
    }

}
