package com.zxt.mq.config.rabbitmq;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 前端连接参数对象
 * </p>
 *
 * @author zxt
 * @since 2022/6/22 17:57
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "FrontConnect对象", description = "前端连接参数对象")
public class FrontConnect {
    /** mq 地址 */
    @ApiModelProperty(value = "mq 地址")
    private String ip;
    /** 连接账户 */
    @ApiModelProperty(value = "连接账户")
    private String username;
    /** 连接密码 */
    @ApiModelProperty(value = "连接密码")
    private String password;
    /** 连接端口 */
    @ApiModelProperty(value = "连接端口")
    private String port;

}
