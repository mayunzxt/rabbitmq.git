package com.zxt.mq.config.callback;

import org.springframework.amqp.core.Message;

/**
 * <p>
 * exchange到queue失败,则回调return(publisher-returns: true)
 * </p>
 *
 * @author zxt
 * @since 2022/7/19 13:40
 */
public interface BaseReturnCallback {

    /**
     * 交换机到队列失败时的回调ReturnCallback
     * 实现此接口实现自定义业务处理
     *
     * @param message    消息对象
     * @param replyCode  返回代码
     * @param replyText  返回信息
     * @param exchange   交换机名称
     * @param routingKey 路由key
     */
    void fail(Message message, int replyCode, String replyText, String exchange, String routingKey);
}
