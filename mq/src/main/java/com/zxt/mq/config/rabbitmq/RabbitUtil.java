package com.zxt.mq.config.rabbitmq;

import com.alibaba.fastjson.JSONObject;
import com.zxt.mq.mvc.log.send.MessageSendLogService;

import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Properties;

import javax.annotation.Resource;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * rabbit工具类
 * </p>
 *
 * @author zxt
 * @since 2022/6/15 11:52
 */
@Component
@Slf4j
public class RabbitUtil {

    @Resource
    RabbitTemplate rabbitTemplate;
    @Resource
    RabbitAdmin    rabbitAdmin;
    @Value("${spring.rabbitmq.host}")
    private String host;
    @Value("${spring.rabbitmq.username}")
    private String userName;
    @Value("${spring.rabbitmq.password}")
    private String password;
    @Value("${spring.rabbitmq.frontPort}")
    private String frontPort;
    @Resource
    MessageSendLogService messageSendLogService;

    /**
     * 发送数据
     *
     * @param exchangeName 交换机名称
     * @param routerKey    路由key
     * @param rabbitData   消息对象
     */
    public void sendMessage(String exchangeName, String routerKey, RabbitData rabbitData) {
        CorrelationData correlationData = new CorrelationData();
        correlationData.setId(rabbitData.getUri());
        //发送记录保存
        messageSendLogService.saveLog(rabbitData, exchangeName, routerKey);
        rabbitTemplate.convertAndSend(exchangeName, routerKey, JSONObject.toJSONString(rabbitData), correlationData);
    }


    /**
     * 验证队列是否存在
     *
     * @param queueName 部门主键
     *
     * @return true 不存在 false 存在
     */
    public boolean validationQueueExist(String queueName) {
        Properties properties = rabbitAdmin.getQueueProperties(queueName);
        return ObjectUtil.isNull(properties);

    }

    /**
     * 创建主题交换机
     */
    public TopicExchange createTopicExchange(String exchangeName) {
        //交换机
        TopicExchange topicExchange = new TopicExchange(exchangeName);
        rabbitAdmin.declareExchange(topicExchange);
        return topicExchange;
    }

    /**
     * 创建队列
     */
    public Queue createQueue(String queueName) {
        //队列
        Queue queue = new Queue(queueName);
        rabbitAdmin.declareQueue(queue);
        return queue;
    }

    /**
     * 绑定交换机和队列
     *
     * @param exchange  主题交换机
     * @param queue     队列
     * @param routerKey 路由key
     */
    public void binding(TopicExchange exchange, Queue queue, String routerKey) {
        rabbitAdmin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(routerKey));
    }

    /**
     * 创建交换机和队列并通过路由key绑定
     *
     * @param exchangeName 主题交换机名称
     * @param queueName    队列名称
     * @param routerKey    路由key
     */
    public void createExchangeAndQueue(String exchangeName, String queueName, String routerKey) {
        binding(createTopicExchange(exchangeName), createQueue(queueName), routerKey);
    }

    public FrontConnect getFrontConnect() {
        return new FrontConnect().setIp(host).setPassword(password).setPort(frontPort).setUsername(userName);
    }
}
