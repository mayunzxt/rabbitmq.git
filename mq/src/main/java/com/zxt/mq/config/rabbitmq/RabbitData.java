package com.zxt.mq.config.rabbitmq;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * mq报文消息结构体
 * </p>
 *
 * @author zxt
 * @since 2022/6/17 16:06
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "RabbitData对象", description = "mq报文消息结构体")
public class RabbitData {
    /** 报文消息唯一标识 */
    @ApiModelProperty(value = "报文消息唯一标识")
    private String  uri;
    /** 发送报文参数对象 json字符串格式 */
    @ApiModelProperty(value = "发送报文参数对象 json字符串格式")
    private String  sendMsg;
    /** 发送时间戳 */
    @ApiModelProperty(value = "发送时间戳")
    private long    sendTimestamp;
    /** 回复时有值，对应请求时uri */
    @ApiModelProperty(value = "回执报文主题")
    private String  reqUri;
    /** 最大重发次数 */
    @ApiModelProperty(value = "最大重发次数")
    private Integer maxSend;
    /** 重发时间间隔(秒) */
    @ApiModelProperty(value = "重发时间间隔(秒)")
    private Integer intervalTime;
}
