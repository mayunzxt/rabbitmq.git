package com.zxt.mq.config.callback;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消息发送到交换机回调,需配置 publisher-confirm-type
 * </p>
 *
 * @author zxt
 * @since 2022/7/19 9:57
 */
@Service
public interface BaseConfirmCallback {

    /**
     * 成功到交换机时的回调
     * 实现此接口实现自定义业务处理
     *
     * @param correlationData 消息数据 correlationData.getId() 业务数据消息唯一标识
     * @param cause           成功或失败原因
     */
    void success(CorrelationData correlationData, String cause);

    /**
     * 交换机失败时的回调
     * 实现此接口实现自定义业务处理
     *
     * @param correlationData 消息数据
     * @param cause           成功或失败原因
     */
    void fail(CorrelationData correlationData, String cause);

}
