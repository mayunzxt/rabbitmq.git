
# MQ框架
## 框架说明
1. **本框架使用SpringBoot+SSM(Spring、SpringMVC、MyBatis)作为基础工作框架，并集成了常用的库并做了必要配置，
使用时可以直接在maven中依赖本库 `compile 'com.zxt:zmq-java:1.0.2''`.**
1. **本框架带有示例项目`UseExample`,使用Gradle构建，新建项目时可参照此项目构建.**
1. **本框架集成的库包括：**
    - SpringBoot+SSM(Spring、SpringMVC、MyBatis)；
    - MyBatisPlus：MyBatis的增强库；
    - druid：阿里德鲁伊连接池；
    - rabbitmq: mq消息中间件
 1. **支持可扩展监听**
    - BaseConfirmCallback 消息发送到交换机回调
    - BaseReturnCallback exchange到queue失败回调
 1. **支持动态队列以及队列消费者**
    - BaseConsumer 消费者抽象实现类
    - 消费者定义参见演示项目 TestService、DynamicConsumer
 1. **支持消息持久化、消息发送失败/未确认消费消息(手工确认模式)重发机制**
    - 配置信息参见 rabbitmq配置文件 消息持久化
    - 持久化服务类 发送记录MessageSendLogService 接收记录 MessageReceiveLogService
    - 重发任务 RetryService、RetryRunner
