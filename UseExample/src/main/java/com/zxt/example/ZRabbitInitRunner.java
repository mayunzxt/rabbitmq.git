package com.zxt.example;


import com.mchange.lang.ThrowableUtils;
import com.zxt.example.mvc.zcommon.consumer.TestService;

import org.springframework.amqp.AmqpException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 服务启动后-rabbitmq的操作
 * </p>
 *
 * @author zxt
 * @since 2022/6/6 13:29
 */
@Slf4j
@Component
public class ZRabbitInitRunner implements ApplicationRunner {
    //    @Resource
    //    RabbitInitService rabbitInitService;
    //    @Resource
    //    RabbitUtil        rabbitUtil;
    @Resource
    TestService testService;

    @Override
    public void run(ApplicationArguments args) {
        try {
            testService.dynamicCreateConsumer();
            // 初始化交换机 队列 绑定
            //            rabbitInitService.init();
            //            //发送数据
            //            RabbitData rabbitData = new RabbitData().setUri(IdUtil.simpleUUID())
            //                                                    .setSendMsg("ceshi shuju message")
            //                                                    .setSendTimestamp(System.currentTimeMillis());
            //            rabbitUtil.sendMessage(RabbitConstants.EXCHANGE + "mq-test", RabbitConstants.QUEUE +
            //            "mq-test", rabbitData);
        } catch (AmqpException e) {
            log.error("rabbitmq init exception {}", ThrowableUtils.extractStackTrace(e));
        }
    }
}
