package com.zxt.example.mvc.zcommon.constants;

/**
 * <p>
 * rabbit的常量定义类
 * </p>
 *
 * @author zxt
 * @since 2022/6/15 13:10
 */
public class RabbitConstants {

    private RabbitConstants() {

    }

    /** 交换机名称前缀 */
    public static final String EXCHANGE = "example-topic-";

    /** 队列名称前缀 */
    public static final String QUEUE = "example-queue-";
}
