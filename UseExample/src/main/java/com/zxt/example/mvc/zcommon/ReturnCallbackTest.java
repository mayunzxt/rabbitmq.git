package com.zxt.example.mvc.zcommon;

import com.alibaba.fastjson.JSONObject;
import com.mchange.lang.ThrowableUtils;
import com.zxt.mq.config.callback.BaseReturnCallback;
import com.zxt.mq.config.rabbitmq.RabbitData;

import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 消息由交换机到队列失败时的业务回调，由spring管理
 * </p>
 *
 * @author zxt
 * @since 2022/7/19 13:51
 */
@Service
@Slf4j
public class ReturnCallbackTest implements BaseReturnCallback {

    /**
     * 交换机到队列失败时的回调
     *
     * @param message    消息对象
     * @param replyCode  返回代码
     * @param replyText  返回信息
     * @param exchange   交换机名称
     * @param routingKey 路由key
     */
    @Override
    public void fail(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        //todo 交换机到队列失败时业务处理，防止消息丢失，可进行把消息重新发送等操作
        try {
            String str = new String(message.getBody(), StandardCharsets.UTF_8);
            RabbitData rabbitData = JSONObject.parseObject(str, RabbitData.class);
        } catch (Exception e) {
            log.debug("消息到队列失败,{}", ThrowableUtils.extractStackTrace(e));
        }

    }
}
