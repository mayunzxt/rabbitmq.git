package com.zxt.example.mvc.zcommon.consumer;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>
 * 用户存放全局消费者
 * </p>
 *
 * @author zxt
 * @since 2022/7/22 11:46
 */
@Component
public class CustomizeDynamicConsumerContainer {

    /** 用于存放全局消费者 */
    public final Map<String, DynamicConsumer> customizeDynamicConsumerContainer = new ConcurrentHashMap<>();
}
