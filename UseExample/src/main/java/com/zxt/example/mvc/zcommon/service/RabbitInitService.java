package com.zxt.example.mvc.zcommon.service;

import com.zxt.example.mvc.zcommon.constants.RabbitConstants;
import com.zxt.mq.config.rabbitmq.RabbitUtil;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * rabbit 交换机以及队列
 * </p>
 *
 * @author zxt
 * @since 2022/6/17 11:09
 */
@Service
public class RabbitInitService {
    @Resource
    RabbitUtil rabbitUtil;

    /**
     * 初始化所有部门的交换机 队列
     */
    public void init() {
        createExchange("mq-test");
    }


    /**
     * 创建交换机 队列 绑定
     *
     * @param exchangeName 交换机名称
     */
    public void createExchange(String exchangeName) {
        String exchange = RabbitConstants.EXCHANGE + exchangeName;
        String queueName = RabbitConstants.QUEUE + exchangeName;
        rabbitUtil.createExchangeAndQueue(exchange, queueName, queueName);
    }

}
