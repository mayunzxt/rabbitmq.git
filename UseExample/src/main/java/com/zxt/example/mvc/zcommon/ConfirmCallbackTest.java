package com.zxt.example.mvc.zcommon;

import com.zxt.mq.config.callback.BaseConfirmCallback;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消息到交换机时的业务回调实现，此回调需交付spring管理
 * </p>
 *
 * @author zxt
 * @since 2022/7/19 13:50
 */
@Service
public class ConfirmCallbackTest implements BaseConfirmCallback {

    /**
     * 成功到交换机时的回调
     *
     * @param correlationData 消息数据
     * @param cause           成功或失败原因
     */
    @Override
    public void success(CorrelationData correlationData, String cause) {
        //todo 成功发送到交换机时的业务处理
    }

    /**
     * 交换机失败时的回调
     *
     * @param correlationData 消息数据
     * @param cause           成功或失败原因
     */
    @Override
    public void fail(CorrelationData correlationData, String cause) {
        //todo 发送到交换机失败时的业务处理
    }
}
