package com.zxt.example.mvc.zcommon.consumer;

import cn.hutool.core.util.IdUtil;
import com.mchange.lang.ThrowableUtils;
import com.zxt.mq.config.rabbitmq.RabbitData;
import com.zxt.mq.config.rabbitmq.RabbitUtil;
import com.zxt.mq.consumer.DynamicConsumerContainerFactory;
import com.zxt.mq.consumer.util.ConsumerContainerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @since 2022/7/22 10:50
 */
@Service
@Slf4j
public class TestService {
    @Resource
    private ConnectionFactory                 connectionFactory;
    @Resource
    private RabbitAdmin                       rabbitAdmin;
    @Resource
    private CustomizeDynamicConsumerContainer customizeDynamicConsumerContainer;
    @Resource
    RabbitUtil rabbitUtil;

    public void dynamicCreateConsumer() {
        Map<String, DynamicConsumer> allQueueContainerMap =
                customizeDynamicConsumerContainer.customizeDynamicConsumerContainer;
        DynamicConsumer consumer;
        try {
            //根据交换机 队列 路由 创建消费者
            DynamicConsumerContainerFactory dynamicConsumerContainerFactory = ConsumerContainerUtil.genConsumer(connectionFactory,
                                                                                                                rabbitAdmin,
                                                                                                                "test001-1",
                                                                                                                "test001",
                                                                                                                "routingKey001",
                                                                                                                false,
                                                                                                                true,
                                                                                                                true);

            consumer = new DynamicConsumer(dynamicConsumerContainerFactory, "test001");
            allQueueContainerMap.put("test001", consumer);
            //启动消费者
            consumer.start();
            //发送消息到交换机
            rabbitUtil.sendMessage("test001-1",
                                   "routingKey001",
                                   new RabbitData().setSendMsg("Hello MQ!").setUri(IdUtil.simpleUUID()));
        } catch (Exception e) {
            log.error("动态添加队列监听异常{}", ThrowableUtils.extractStackTrace(e));
            log.error("系统异常", e);
        }
    }

    /**
     * 暂停消费者
     */
    public void stop() {
        Map<String, DynamicConsumer> allQueueContainerMap =
                customizeDynamicConsumerContainer.customizeDynamicConsumerContainer;
        DynamicConsumer dynamicConsumer = allQueueContainerMap.get("test001");
        dynamicConsumer.stop();
    }
}
