package com.zxt.example.config.mvc;

import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.zstk.frame.config.ZFrameConfig;
import com.zstk.frame.util.upload.ZUploadUtil;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.ArrayList;
import java.util.List;

import static com.alibaba.fastjson.serializer.SerializerFeature.DisableCircularReferenceDetect;
import static com.alibaba.fastjson.serializer.SerializerFeature.PrettyFormat;
import static com.alibaba.fastjson.serializer.SerializerFeature.QuoteFieldNames;
import static com.alibaba.fastjson.serializer.SerializerFeature.SkipTransientField;
import static com.alibaba.fastjson.serializer.SerializerFeature.WriteDateUseDateFormat;
import static com.alibaba.fastjson.serializer.SerializerFeature.WriteMapNullValue;
import static com.alibaba.fastjson.serializer.SerializerFeature.WriteNullBooleanAsFalse;
import static com.alibaba.fastjson.serializer.SerializerFeature.WriteNullListAsEmpty;
import static com.alibaba.fastjson.serializer.SerializerFeature.WriteNullNumberAsZero;
import static com.alibaba.fastjson.serializer.SerializerFeature.WriteNullStringAsEmpty;

/**
 * 数据转换配置，json转换为对象及对象转换为json
 *
 * @since 2019/7/31 17:53
 */
@Configuration
public class MvcConfigSupport extends WebMvcConfigurationSupport {

    /**
     * 虚拟路径映射文件磁盘路径 读取磁盘文件 预览文件
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(ZFrameConfig.getInstance().getUploadPath())
                .addResourceLocations("file:" + ZUploadUtil.getUploadFilePath());
        //swagger接口映射
        registry.addResourceHandler("/doc.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    /**
     * 为MVC定制Object的默认转换器
     * 解析浏览器的请求JSON数据为对象并将返回对象类型转为JSON字符串
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        FastJsonConfig config = new FastJsonConfig();
        config.setSerializerFeatures(
                // 输出key时是否使用双引号，默认为true
                QuoteFieldNames,
                // 是否输出值为null的字段, 默认为false
                WriteMapNullValue,
                // 数值字段如果为null,输出为0,而非null
                WriteNullNumberAsZero,
                // List字段如果为null,输出为[],而非null
                WriteNullListAsEmpty,
                // 字符类型字段如果为null,输出为"",而非null
                WriteNullStringAsEmpty,
                // Boolean字段如果为null,输出为false,而非null
                WriteNullBooleanAsFalse,
                //序列化时忽略transient的字段，默认为true
                SkipTransientField,
                //按字段名称排序,默认false
                //SortField,
                // 全局修改日期格式,默认为false
                WriteDateUseDateFormat,
                //结果是否格式化,默认为false
                PrettyFormat,
                // 消除对同一对象循环引用的问题，默认为false
                DisableCircularReferenceDetect);
        config.setDateFormat("yyyy-MM-dd HH:mm:ss");
        converter.setFastJsonConfig(config);

        //解决中文乱码
        List<MediaType> fastMediaTypes = new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        converter.setSupportedMediaTypes(fastMediaTypes);
        converters.add(converter);
        super.addDefaultHttpMessageConverters(converters);
    }

}