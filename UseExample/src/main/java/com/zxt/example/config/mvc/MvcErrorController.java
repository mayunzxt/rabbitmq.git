package com.zxt.example.config.mvc;

import com.zstk.frame.MvcResult;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


/**
 * 发生全局未捕获异常后，springboot会捕获到并且转向到error页面，此处重写error页面的返回值。
 *
 */
@RestController
public class MvcErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == 401) {
            return MvcResult.fail(MvcResult.STATUS_UNAUTH_401, "未登录").toString();
        } else if (statusCode == 404) {
            return MvcResult.fail(MvcResult.STATUS_NOT_FOUND, "地址不存在").toString();
        } else if (statusCode == 403) {
            return MvcResult.fail(MvcResult.STATUS_UNAUTH_403, "无权限").toString();
        } else {
            return MvcResult.fail(MvcResult.STATUS_SERVER_ERROR, "服务端异常").toString();
        }
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}