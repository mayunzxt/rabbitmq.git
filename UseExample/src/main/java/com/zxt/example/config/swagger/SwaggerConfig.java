package com.zxt.example.config.swagger;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger2配置文件
 *
 * @since 2019/7/31 17:53
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {
    @Value("${app-config.swagger-base-dir}")
    private String baseLocation;

    @Value("${app-config.swagger-enabled}")
    private boolean swaggerEnabled;

    @Bean
    public Docket messageLog() {
        return getGroup("message", "消息记录");
    }

    private Docket getGroup(String path, String groupName) {
        path = StringUtils.isEmpty(path) ? baseLocation : baseLocation + "." + path;
        groupName = StringUtils.isEmpty(groupName) ? "全部接口" : groupName;
        return new Docket(DocumentationType.SWAGGER_2).enable(swaggerEnabled)
                                                      .groupName(groupName)
                                                      .apiInfo(apiinfo())
                                                      .select()
                                                      .apis(RequestHandlerSelectors.basePackage(path))
                                                      .paths(PathSelectors.any())
                                                      .build();
    }

    private ApiInfo apiinfo() {
        return new ApiInfoBuilder().title("接口文档").build();
    }
}