package com.zxt.example.config.mybatisplus;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 设置Mybatis Mapper拦截器，拦截Mapper数据库操作，对Mapper进行增强
 * <p>
 * 具体配置详见application-mybatisplus.yml
 * @since 2019/7/31 18:25
 */
@Configuration
@ConditionalOnClass(PaginationInterceptor.class)
public class MyBatisPlusConfig {
    /** 数据类型 */
    @Value("${spring.datasource.dbtype}")
    private String dbtype;

    /**
     * 如需分页时，通过形参向Mapper接口注入分页对象Pagination，根据方言调整SQL语句进行分页 ；
     * 分页操作可以参考示例DemoService
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor page = new PaginationInterceptor();
        page.setDialectType(dbtype);
        return page;
    }
}
